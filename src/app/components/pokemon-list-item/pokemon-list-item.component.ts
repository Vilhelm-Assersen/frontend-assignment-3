import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemonResponse.model';
import { environment } from 'src/environments/environment';
const { apiSprites } = environment;

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent {

  @Input() pokemon?: Pokemon;

  getImage: string ="";

  constructor() { }

  ngOnInit(): void {
    if (this.pokemon){
      let id = this.pokemon.url.split("/").slice(-2,-1)[0]
      this.getImage = `${apiSprites}${id}.gif`
    }
  }
}