import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { PokemonCollectionService } from 'src/app/services/pokemon-collection.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})
export class CatchButtonComponent implements OnInit {

  public loading: boolean = false;
  public isCaught: boolean = false;
  @Input() pokemonName: string = "";

  constructor(
    private trainerService: TrainerService,
    private readonly pokemonCollectionService: PokemonCollectionService
  ) { }

  ngOnInit(): void {
    this.isCaught = this.trainerService.inCollection(this.pokemonName)
  }

  // Adding pokemon to the trainers collection
  onCatchClick(): void {
    this.loading = true;
    this.pokemonCollectionService.addToCollection(this.pokemonName)
      .subscribe({
        next: (trainer: Trainer) => {
          this.loading = false;
          this.isCaught = this.trainerService.inCollection(this.pokemonName)
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message);
        }
      })
  }
}
