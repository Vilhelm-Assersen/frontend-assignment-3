import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, switchMap, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';
const { apiTrainers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependency injection
  constructor(private readonly http: HttpClient) { }

  // Model, HttpClient, Observables, and RxJS operators
  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username)
      .pipe(
        switchMap((trainer: Trainer | undefined) => {
          if (trainer === undefined) { //Trainer does not exists
            return this.createTrainer(username);
          }
          return of(trainer);
        })
      )
  }

  // Login

  // check if Trainer exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`)
      .pipe(
        // RxJS operators
        map((response: Trainer[]) => response.pop())
      )
  }

  // IF NOT Trainer - create user
  private createTrainer(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: []
    };

    // Headers -> API key
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });

    // POST - Create items on the server
    return this.http.post<Trainer>(apiTrainers, trainer, {
      headers
    })
  }

  // IF Trainer || Created Used -> tore user
}
