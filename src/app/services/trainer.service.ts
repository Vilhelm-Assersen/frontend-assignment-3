import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemonResponse.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  public inCollection(pokemonName: string): boolean {
    if (this._trainer) {

      return Boolean(this.trainer?.pokemonCollection.find((pokemon: Pokemon) => pokemon.name === pokemonName))
    }
    return false;
  }

  public catchPokemon(pokemon: Pokemon): void {
    if (this._trainer) {
      this._trainer.pokemonCollection.push(pokemon);
    }
  }


  public releasePokemon(pokemonName: string): void {
    if (this._trainer) {
      this._trainer.pokemonCollection = this._trainer.pokemonCollection.filter((pokemon: Pokemon) => pokemon.name !== pokemonName);
    }
  }
}
