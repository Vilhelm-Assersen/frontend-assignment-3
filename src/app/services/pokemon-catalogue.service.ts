import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonResponse, Pokemon } from '../models/pokemonResponse.model';
import { StorageUtil } from '../utils/storage.utils';

const { apiPokemon, apiSprites } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemon: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemon;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading
  }

  constructor(private readonly http: HttpClient) { }

  //Gathers the JSON data from all the individual pokemon supplied by the api
  public findAllPokemons(): void {

    if (this.pokemons.length > 0 || this.loading) {
      return;
    }

    this._loading = true;
    this.http.get<PokemonResponse>(apiPokemon)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )

      .subscribe({
        next: (result: PokemonResponse) => {
          this._pokemon = result.results;
          StorageUtil.storageSave("pokemon", this._pokemon);
          this._pokemon.forEach(pokemon => {
            pokemon.id = pokemon.url.split("/", -1).slice(-2, -1)[0]
            pokemon.image = `${apiSprites}${pokemon.id}.gif`
          });
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;

        }
      })
  }

  //gets the json data from a specific pokemon
  public findPokemonId(name: string): Pokemon | undefined {
    console.log(typeof name);
    console.log(name);

    return this.pokemons.find((pokemon: Pokemon) => pokemon.name == name);
  }
}
