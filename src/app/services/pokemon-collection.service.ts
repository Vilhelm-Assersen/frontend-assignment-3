import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemonResponse.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

const { apiKey, apiTrainers } = environment

@Injectable({
  providedIn: 'root'
})
export class PokemonCollectionService {


  constructor(
    private readonly http: HttpClient,
    private readonly pokemonCollection: PokemonCatalogueService,
    private readonly trainerService: TrainerService,
  ) { }

  //Adds the pokemon to the users collection (catches the pokemon)
  public addToCollection(pokemonName: string): Observable<Trainer> {

    if (!this.trainerService.trainer) {
      throw new Error("addToCollection: There is no pokemon");
    }

    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonCollection.findPokemonId(pokemonName);

    if (!pokemon) {
      throw new Error("No pokemon with id: " + pokemonName)
    }

    if (this.trainerService.inCollection(pokemonName)) {
      this.trainerService.releasePokemon(pokemonName)
    } else {
      this.trainerService.catchPokemon(pokemon)
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, {
      pokemonCollection: [...trainer.pokemonCollection]
    }, {
      headers
    })
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer;
        })
      )
  }

}
