import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemonResponse.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { StorageUtil } from 'src/app/utils/storage.utils';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemons(): Pokemon[] | undefined {
    return StorageUtil.storageRead("pokemon");
  }

  get loading(): boolean {
    return this.pokemonCatalogueService.loading;
  }

  get error(): string {
    return this.pokemonCatalogueService.error;
  }

  constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }

  ngOnInit(): void {
    this.pokemonCatalogueService.findAllPokemons();
  }

}
