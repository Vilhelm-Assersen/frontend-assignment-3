export class StorageUtil {

    //Saves the user to the session storage
    public static storageSave<T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

    //Gets the data of the user from the session storage
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);
        try {
            if (storedValue) {
                return JSON.parse(storedValue)
            }
            return undefined;
        }
        catch (e) {
            sessionStorage.removeItem(key);
            return undefined;
        }
    }
}
