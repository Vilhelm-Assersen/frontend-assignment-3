export interface PokemonResponse {
    count: number;
    next: string;
    previous?: any;
    results: Pokemon[];
}

export interface Pokemon {
    id: string;
    url: string;
    name: string;
    image: string;
}