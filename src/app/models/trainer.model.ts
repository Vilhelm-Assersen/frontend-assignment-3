import { Pokemon } from "./pokemonResponse.model";

export interface Trainer {
    id: number;
    username: string;
    pokemonCollection: Pokemon[];
}