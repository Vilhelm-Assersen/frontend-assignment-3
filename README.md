# Lost in Translation

## what you need
To run the app simply go to, "https://frontend-assignment-3-kappa.vercel.app/", where the app is deployed. Otherwise run the command "ng serve" in the terminal in the folder containing the program files

## Motivation
In a desire to become a fullstack developer, this is a great step forward in order to learn frontend development. This project was a chance to better learn frontend basics as well as using Angular. Angular is a useful tool that all frontend developers should be familiar with. Even though it's old compared to most frameworks it's still used by many companies.

## Why I created the Pokemon app
By creating a pokemon app we had a lot of small problems that were all useful learning experiences. Such as creating a login page that verifies if you're already logged in or not. A pokemon catalogue page that fetches pokemon data and returns the appropriate pokemon as an image and name. A profile page that stores the users caught pokemon in a log even if the user logs out or refreshes the page.

## Credits
All the credit of this app goes to me, @Vilhelm-Assersen